# eu4-arabic-mod (مود اللغة العربية)

#### Translation نسبة الترجمة الكلية:

| النسبة |
| ------ |
| 9% |


### :speech_balloon: Files Translated (ملفات ترجمت):

| File Name (اسم الملف) | Progress (الإنجاز)|
| ---------------- | ---------------- |
- [ ] achievements_l_english.yml ---------- {- 0% -}
- [ ] achievement_tooltips_l_english.yml ---------- {- 0% -} 
- [ ] aow_l_english.yml ---------- {+ 1% +} 
- [ ] areas_regions_l_english.yml ---------- {+ 90% +} 
- [ ] common_sense_l_english.yml ---------- {+ 1% +} 
- [x] conquest_of_constantinople_l_english.yml ---------- {+ 100% +} 
- [ ] core_l_english.yml ---------- {+ 2% +} 
- [ ] cossacks_l_english.yml ---------- {+ 1% +} 
- [ ] countries_l_english.yml ---------- {+ 80% +} 
- [ ] cradle_of_civilization_l_english.yml ---------- {+ 1% +} 
- [ ] cultures_phase4_l_english.yml ---------- {- 0% -} 
- [ ] custom_localisation_l_english.yml ---------- {- 0% -} 
- [ ] decisions_l_english.yml ---------- {- 0% -} 
- [ ] diplomacy_l_english.yml ---------- {+ 1% +} 
- [ ] diplomatic_action_l_english.yml ---------- {+ 20% +} 
- [ ] diplo_reasons_l_english.yml ---------- {+ 1% +} 
- [ ] eldorado_l_english.yml ---------- {+ 1% +} 
- [ ] EU4_l_english.yml ---------- {+ 1% +} 
- [x] europa_universalis_iv_sabaton_soundtrack_l_english.yml ---------- {+ 100% +} 
- [ ] flavor_events_l_english.yml ---------- {+ 1% +} 
- [ ] fredmansepistles_l_english.yml ---------- {- 0% -} 
- [ ] fredmansmidsummerepistles_l_english.yml ---------- {- 0% -} 
- [ ] generic_events_l_english.yml ---------- {+ 1% +} 
- [ ] government_names_l_english.yml ---------- {+ 80% +} 
- [ ] hinduism_l_english.yml ---------- {- 0% -} 
- [ ] hints_l_english.yml ---------- {+ 1% +} 
- [ ] institutions_l_english.yml ---------- {- 0% -} 
- [x] kairismusicp2_l_english.yml ---------- {+ 100% +} 
- [x] kairismusic_l_english.yml ---------- {+ 100% +} 
- [ ] languages.yml ---------- {- 0% -} 
- [ ] ledger_l_english.yml ---------- {+ 1% +} 
- [ ] loadingtips_l_english.yml ---------- {- 0% -} 
- [ ] maghreb_formations_l_english.yml ---------- {- 0% -} 
- [ ] mandate_of_heaven_l_english.yml ---------- {+ 1% +} 
- [ ] mare_nostrum_l_english.yml ---------- {+ 1% +} 
- [ ] mercantilism_l_english.yml ---------- {- 0% -} 
- [ ] messages_l_english.yml ---------- {+ 10% +} 
- [ ] missing_correct_english_tut_hint.txt
- [ ] missions_l_english.yml ---------- {- 0% -} 
- [ ] modifers_l_english.yml ---------- {- 0% -} 
- [x] musicplayer_l_english.yml ---------- {+ 100% +} 
- [ ] muslim_dlc_l_english.yml ---------- {- 0% -} 
- [ ] native_flavor_events_l_english.yml ---------- {- 0% -} 
- [ ] new_missions_l_english.yml ---------- {- 0% -} 
- [x] notlocalized_l_english.yml ---------- {+ 100% +} 
- [x] nudge_l_english.yml ---------- {+ 100% +} 
- [ ] nw2_l_english.yml ---------- {+ 1% +} 
- [ ] nw_l_english.yml ---------- {- 0% -} 
- [ ] opinions_l_english.yml ---------- {- 0% -} 
- [x] ottoman_tunes_l_english.yml ---------- {+ 100% +} 
- [ ] personalityoptions_l_english.yml ---------- {- 0% -} 
- [ ] powers_and_ideas_l_english.yml ---------- {- 0% -} 
- [ ] privateers_l_english.yml ---------- {- 0% -} 
- [ ] prov_names_adj_l_english.yml ---------- {+ 50% +} 
- [ ] prov_names_l_english.yml ---------- {- 0% -} 
- [ ] Purple_Phoenix_l_english.yml ---------- {- 0% -} 
- [ ] random_new_world_l_english.yml ---------- {- 0% -} 
- [ ] regions_phase4_l_english.yml ---------- {- 0% -} 
- [ ] religion_l_english.yml ---------- {- 0% -} 
- [x] republicanmusic_l_english.yml ---------- {+ 100% +} 
- [ ] res_publica_l_english.yml ---------- {+ 1% +} 
- [ ] rights_of_man_l_english.yml ---------- {+ 1% +} 
- [ ] rule_britannia_l_english.yml ---------- {- 0% -} 
- [x] rus_awaken_l_english.yml ---------- {+ 100% +} 
- [ ] sikh_l_english.yml ---------- {- 0% -} 
- [x] songsofregency_l_english.yml ---------- {+ 100% +} 
- [x] songs_of_exploration_l_english.yml ---------- {+ 100% +} 
- [x] songs_of_the_new_world_l_english.yml ---------- {+ 100% +} 
- [x] songs_of_war_l_english.yml ---------- {+ 100% +} 
- [x] songs_of_yuletide_l_english.yml ---------- {+ 100% +} 
- [ ] startup_screen_l_english.yml ---------- {+ 1% +} 
- [ ] subject_type_l_english.yml ---------- {- 0% -} 
- [ ] tags_phase4_l_english.yml ---------- {+ 45% +} 
- [ ] technology_l_english.yml ---------- {- 0% -} 
- [x] terrain_stuff_l_english.yml ---------- {+ 100% +} 
- [ ] text_l_english.yml ---------- {+ 3% +} 
- [ ] text_l_english_0_815.yml ---------- {- 0% -} 
- [ ] third_rome_l_english.yml ---------- {+ 1% +} 
- [ ] tradenodes_l_english.yml ---------- {+ 1% +} 
- [ ] triggers_and_effects_l_english.yml ---------- {- 0% -} 
- [ ] tutorial_l_english.yml ---------- {+ 7% +} 
- [ ] USA_dlc_l_english.yml ---------- {- 0% -} 
- [ ] womeninhistory_l_english.yml ---------- {- 0% -} 
